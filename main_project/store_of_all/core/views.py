from django.views.generic.base import TemplateView
from core.models import Product, Category


class IndexView(TemplateView):
    template_name = 'index.html'
    
    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        # products = Product.objects.filter(
        #     name='Rolton',
        #     category=Category.objects.get(id=1)
        # )
        # products = Product.objects.filter(name__in=['Tesla', 'Rolton'])

        products = Product.objects.filter(category__name='Food')

        print(products)
        return context


