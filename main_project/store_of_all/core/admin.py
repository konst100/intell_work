from django.contrib import admin
from core.models import Product, Category, Tags


admin.site.register(Product)
admin.site.register(Category)
admin.site.register(Tags)
