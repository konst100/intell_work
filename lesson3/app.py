from flask import Flask, render_template, request

app = Flask(__name__)


@app.route('/')
def hello_world():
    with open('db.txt') as db:
        data = db.readlines()
        data = [x for x in data if x.startswith(request.values['name'])]
        return render_template(
            'index.html', **{
                'username': "Kivi",
                'names': data,
                'query': request.values
            }
        )


@app.route('/about/')
def about_us():
    return "About us"


if __name__ == '__main__':
    app.run(
        host='0.0.0.0',
        port=5000,
        debug=True
    )
