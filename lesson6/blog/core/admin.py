from django.contrib import admin
from core.models import Post, Comment, Tags


admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(Tags)
