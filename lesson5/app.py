import sqlite3
from peewee import DoesNotExist
from flask import Flask, render_template, request, redirect, abort
from models import Song, Author, Genre
from forms import SongForm

app = Flask(__name__)


# connection = sqlite3.connect('db.db')
# cursor = connection.cursor()


def execute_query(query):
    return 12


'''
SELECT 
    "t1"."id", "t1"."name", "t1"."duration", "t1"."author_id", "t1"."genre_id" 
FROM 
    "song" AS "t1" 
INNER JOIN 
    "author" AS "t2" ON ("t1"."author_id" = "t2"."id") 
INNER JOIN 
    "genre" AS "t3" ON ("t1"."genre_id" = "t3"."id")
'''


@app.route('/')
def song_list():
    songs = Song.select().join_from(Song, Author).join_from(Song, Genre)
    return render_template('index.html', songs=songs)


@app.route('/<int:pk>/')
def song_detail(pk):
    # author = Author.select().where(Author.id == pk and Author.name.startswith("W")).get()
    try:
        author = Author.select().where(Author.id == pk).get()
    except DoesNotExist:
        return abort(404)
    # songs = Song.select().where(Song.author == pk)
    return render_template('author.html', author=author)


@app.route('/genre/<int:pk>/')
def songs_by_genre(pk):
    songs = execute_query(f"SELECT * FROM songs WHERE genre={pk}")
    return render_template('index.html', songs=songs)


@app.route('/add/artist/', methods=['GET', 'POST'])
def add_artist():
    if request.method == 'POST':
        # execute_query(
        #     f"INSERT INTO artist (name) VALUES ('{request.form['name']}')"
        # )

        # TODO v1
        # author = Author(name=request.form['name'])
        # author.save()

        Author.create(name=request.form['name'])

        return redirect('/')
    return render_template('add_artist.html')


@app.route('/remove_song/', methods=['POST'])
def remove_song():
    Song.delete().where(Song.id == int(request.form['id'])).execute()
    return redirect('/')


@app.route('/add/song/', methods=['GET', 'POST'])
def song_create():
    form = SongForm(obj=Song.select().get())
    if request.method == 'POST':
        form = SongForm(formdata=request.form)
        if form.validate():
            form.save()
            return redirect('/')

    return render_template('add_song.html', form=form)


if __name__ == '__main__':
    app.run(
        host='0.0.0.0',
        port=5000,
        debug=True
    )
