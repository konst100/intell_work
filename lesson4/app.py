import sqlite3
from flask import Flask, render_template, request, redirect

app = Flask(__name__)


# connection = sqlite3.connect('db.db')
# cursor = connection.cursor()


def execute_query(query):
    with sqlite3.connect('db.db') as conn:
        cursor = conn.cursor()
        cursor.execute(query)
        return cursor.fetchall()


def get_by_id(table, record_id):
    return execute_query(
        f"SELECT * FROM {table} WHERE id={record_id}"
    )[0]


@app.route('/')
def song_list():
    songs = execute_query("SELECT * FROM songs")
    return render_template('index.html',
                           songs=songs, get_by_id=get_by_id)


@app.route('/<int:pk>/')
def song_detail(pk):
    songs = execute_query(f"SELECT * FROM songs WHERE author={pk}")
    return render_template('index.html',
                           songs=songs, get_by_id=get_by_id)


@app.route('/genre/<int:pk>/')
def songs_by_genre(pk):
    songs = execute_query(f"SELECT * FROM songs WHERE genre={pk}")
    return render_template('index.html',
                           songs=songs, get_by_id=get_by_id)


@app.route('/add/artist/', methods=['GET', 'POST'])
def add_artist():
    if request.method == 'POST':
        execute_query(
            f"INSERT INTO artist (name) VALUES ('{request.form['name']}')"
        )
        return redirect('/')
    return render_template('add_artist.html')


@app.route('/remove_song/', methods=['POST'])
def remove_song():
    execute_query(
        f"DELETE FROM songs WHERE id={request.form['id']}"
    )
    return redirect('/')


if __name__ == '__main__':
    app.run(
        host='0.0.0.0',
        port=5000,
        debug=True
    )
